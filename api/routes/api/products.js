const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const Product = require("../../models/Product");

// @route   Product api/products
// @desc    Create a Product
// @access

router.post(
	"/",
	[
		check("name", "Name is required")
			.not()
			.isEmpty(),
		check("priceUSD", "Price is must be numeric").isNumeric(),
		check("priceUSD", "Price is required")
			.not()
			.isEmpty()
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const { name, priceUSD } = req.body;
		const productFields = {};
		if (name) productFields.name = name;
		if (priceUSD) productFields.priceUSD = priceUSD;

		try {
			if (req.body._id) {
				let product = await Product.findOne({ _id: req.body._id });
				product = await Product.findOneAndUpdate(
					{ _id: req.body._id },
					{ $set: productFields }
				);
				return res.json(product);
			}
			product = new Product(productFields);
			await product.save();
			res.json(product);
		} catch (err) {
			res.status(500).send(err.message);
		}
	}
);

// @route   GET api/products
// @desc    Get all products
// @access  Public

router.get("/", async (req, res) => {
	try {
		const products = await Product.find().sort({ date: -1 });
		res.json(products);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

// @route   GET api/products/:id
// @desc    Get single product
// @access  Public

router.get("/:id", async (req, res) => {
	try {
		const product = await Product.findById(req.params.id);
		if (!product) {
			return res.status(404).json({ msg: "Product not found" });
		}
		res.json(product);
	} catch (err) {
		res.status(404).json(err.message);
	}
	res.status(500).send("api/products: Server Error");
});

//	@route		DELETE api/product/:id
//	@desc			Delete single product
//	@acces		Private
router.delete("/:id", async (req, res) => {
	try {
		await Product.findOneAndRemove({ _id: req.params.id });
		const products = await Product.find().sort({ date: -1 });

		res.json(products);
	} catch (err) {
		res.status(500).send(err.message);
	}
});

module.exports = router;
