const express = require('express');
const path = require('path');
const connectDB = require('./config/db');
const app = express();
app.get('/', (req, res) => {
	res.send('App is running successfully');
});
connectDB();

app.use(express.json({ extended: false }));

app.use('/api/products', require('./routes/api/products'));

// Serve static assets if it in Prodcution
if (process.env.NODE_ENV === 'production') {
	//Set static folder
	app.use(express.static('client/build'));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}
const PORT = process.env.PORT || 8000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
