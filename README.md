#Status of this application

1. I am currently working to Dockerize this application. It is showing some issues, when I run `docker-compose up` from root directory.

#To run this project:

1. Clone from git repo link: [Bitbucket repo](https://gurisandhu@bitbucket.org/gurisandhu/aone-fashion.git)
2. Open this application in code editor
3. In terminal change directory to `api`, run command: `npm install && npm run clientInstall`
4. After that install finished, run: `npm run dev`

# It will,

Runs the app in the development mode.<br> Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
