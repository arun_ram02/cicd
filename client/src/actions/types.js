export const GET_PRODUCTS = "GET_PRODUCTS";
export const GET_PRODUCT = "GET_PRODUCT";
export const GET_ERRORS = "GET_ERRORS";
export const SET_LOADING = "SET_LOADING";
export const GET_CURRENCY_RATES = "GET_CURRENCY_RATES";
export const SET_CURRENCY = "SET_CURRENCY";
