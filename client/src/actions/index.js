import axios from "axios";
import {
	GET_PRODUCTS,
	GET_ERRORS,
	SET_LOADING,
	GET_PRODUCT,
	GET_CURRENCY_RATES,
	SET_CURRENCY
} from "./types";

//eslint-disable-next-line
// import dummyData from "../dummyData/products.json";

export const getProducts = () => async dispatch => {
	try {
		dispatch(setLoading(true));
		const response = await axios.get("/api/products");
		// const response = dummyData.data;
		dispatch({
			type: GET_PRODUCTS,
			payload: response.data
		});
		dispatch(setLoading(false));
	} catch (err) {
		dispatch(getErrors(err.response));
		dispatch(setLoading(false));
	}
};

export const getProduct = id => async dispatch => {
	try {
		dispatch(setLoading(true));
		const res = await axios.get(`/api/products/${id}`);
		dispatch({
			type: GET_PRODUCT,
			payload: res.data
		});
		dispatch(setLoading(false));
	} catch (err) {
		dispatch(getErrors(err.response));
		dispatch(setLoading(false));
	}
};

export const createProduct = (
	formData,
	history,
	edit = false
) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};

	try {
		dispatch(setLoading(true));
		const res = await axios.post("/api/products", formData, config, edit);
		dispatch({
			type: GET_PRODUCT,
			payload: res.data
		});
		if (edit) {
			history.push(`/view/${formData._id}`);
		} else {
			history.push("/");
		}

		dispatch(setLoading(false));
	} catch (err) {
		dispatch(getErrors(err.response.data.errors));
		dispatch(setLoading(false));
	}
};

export const updateProduct = (id, formData, history) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};

	try {
		dispatch(setLoading(true));

		const res = await axios.post(`/api/posts/${id}`, formData, config);
		dispatch({
			type: GET_PRODUCT,
			payload: res.data
		});
		history.push("/");
		dispatch(setLoading(false));
	} catch (err) {
		dispatch(getErrors(err.response.data.errors));
		dispatch(setLoading(false));
	}
};

export const deleteProduct = (id, history) => async dispatch => {
	if (window.confirm("Are you sure? This can NOT be undone!")) {
		try {
			dispatch(setLoading(true));

			const res = await axios.delete(`/api/products/${id}`);
			dispatch({
				type: GET_PRODUCTS,
				payload: res.data
			});
			history.push("/");
			dispatch(setLoading(false));
		} catch (err) {
			dispatch(getErrors(err.response));
			dispatch(setLoading(false));
		}
	}
};

export const getCurrencyRates = () => async dispatch => {
	try {
		dispatch(setLoading(true));

		const res = await axios.get(
			"https://api.exchangeratesapi.io/latest?symbols=USD&base=AUD"
		);
		dispatch({
			type: GET_CURRENCY_RATES,
			payload: res.data.rates
		});
		dispatch(setLoading(false));
	} catch (err) {
		dispatch(getErrors(err.response));
		dispatch(setLoading(false));
	}
};

export const setCurrency = selectedCurrency => dispatch => {
	dispatch({
		type: SET_CURRENCY,
		payload: selectedCurrency
	});
};

export const setLoading = value => dispatch => {
	dispatch({
		type: SET_LOADING,
		payload: value
	});
};
export const getErrors = errors => dispatch => {
	dispatch({
		type: GET_ERRORS,
		payload: errors
	});
};
