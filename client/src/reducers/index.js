import { combineReducers } from "redux";
import products from "./products";
import loading from "./loading";
import errors from "./errors";

export default combineReducers({
	products,
	loading,
	errors
});
