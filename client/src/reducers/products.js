import {
	GET_PRODUCTS,
	GET_PRODUCT,
	GET_CURRENCY_RATES,
	SET_CURRENCY
} from "../actions/types";

const initialState = {
	list: [],
	product: null,
	currencyRates: {},
	currency: "AUD"
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_PRODUCTS:
			return {
				...state,
				list: payload
			};
		case GET_PRODUCT:
			return {
				...state,
				product: payload
			};
		case GET_CURRENCY_RATES:
			return {
				...state,
				currencyRates: payload
			};
		case SET_CURRENCY:
			return {
				...state,
				currency: payload
			};
		default:
			return state;
	}
}
