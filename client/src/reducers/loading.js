import { SET_LOADING } from "../actions/types";

const initialState = {
	status: false
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case SET_LOADING:
			return {
				...state,
				status: payload
			};
		default:
			return state;
	}
}
