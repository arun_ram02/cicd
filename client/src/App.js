import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import store from './store';
import { Dashboard, Header } from './components';
import ErrorBoundary from './ErrorBoundary';
import Routes from './routing/Routes';

function App() {
	return (
		<ErrorBoundary>
			<Provider store={store}>
				<Router>
					<Header />
					<Switch>
						<Route exact path="/" component={Dashboard} />
						<Route component={Routes} />
					</Switch>
				</Router>
			</Provider>
		</ErrorBoundary>
	);
}

export default App;
