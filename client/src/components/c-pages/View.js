import React, { useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Loader } from "../";
import { getProduct, deleteProduct } from "../../actions";
import { isEmpty } from "../../utls";

const ViewPost = ({
	history,
	deleteProduct,
	getProduct,
	match,
	loading,
	products: { product }
}) => {
	useEffect(() => {
		getProduct(match.params.id);
	}, [getProduct, match.params.id]);

	return loading || isEmpty(product) ? (
		<Loader fullScreen />
	) : (
		<>
			<div className="wrapper content-area white">
				<div className="row center">
					<div className="left column">
						<h3>{product.name}</h3>
						<h2>${product.priceUSD}</h2>
					</div>
					<Link to={`/edit/${product._id}`} className="buttonTwo">
						Edit
					</Link>
					<button
						className="buttonTwo danger"
						onClick={() => deleteProduct(product._id, history)}>
						Delete
					</button>
				</div>
			</div>
			<div className="wrapper">
				<div className="row">
					<Link to="/" className="buttonOne">
						&lt; Back
					</Link>
				</div>
			</div>
		</>
	);
};

const mapStateToProps = state => ({
	products: state.products,
	loading: state.loading.status
});

export default connect(
	mapStateToProps,
	{ getProduct, deleteProduct }
)(withRouter(ViewPost));
