import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getProducts, getCurrencyRates, setCurrency } from "../../actions";
import { Item, Loader } from "../";
import { isEmpty } from "../../utls";

function Dashboard({
	getProducts,
	loading,
	products,
	errors,
	getCurrencyRates,
	setCurrency
}) {
	const { list, currency } = products;
	useEffect(() => {
		getProducts();
		getCurrencyRates();
	}, [getCurrencyRates, getProducts]);
	return (
		<div className="wrapper">
			<div className="container">
				<section className="product-list" role="main">
					{loading || isEmpty(products.list) ? (
						<Loader fullScreen={false} />
					) : list ? (
						<>
							<div className="product-item">
								<div className="filterWrapper">
									<button
										className={`buttonOne ${currency === "USD" && "active"}`}
										onClick={() => setCurrency("USD")}>
										USD
									</button>
									<button
										className={`buttonOne ${currency === "AUD" && "active"}`}
										onClick={() => setCurrency("AUD")}>
										AUD
									</button>
								</div>
							</div>
							{list.map(item => (
								<Item key={item._id} item={item} />
							))}
						</>
					) : errors ? (
						<div className="wrapper center">
							<h3>{errors.message}</h3>
						</div>
					) : (
						<div className="wrapper center">
							<h3>No Product found</h3>
						</div>
					)}
				</section>
			</div>
		</div>
	);
}

const mapStateToProps = state => ({
	products: state.products,
	loading: state.loading.status,
	errors: state.errors.list
});

export default connect(
	mapStateToProps,
	{ getProducts, getCurrencyRates, setCurrency }
)(Dashboard);

Dashboard.propTypes = {
	getProducts: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
	products: PropTypes.object.isRequired,
	errors: PropTypes.array
};
