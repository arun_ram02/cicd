import React, { useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createProduct, getProduct } from '../../actions';
import { isEmpty } from '../../utls';

function Edit({
	createProduct,
	history,
	products,
	loading,
	errors,
	match,
	getProduct
}) {
	useEffect(() => {
		getProduct(match.params.id);
	}, [getProduct, match.params.id]);
	const { product } = products;
	const [formData, setFormData] = useState({
		name: !isEmpty(products.product) && product.name,
		priceUSD: !isEmpty(products.product) && product.priceUSD,
		_id: !isEmpty(products.product) && product._id
	});

	const { name, priceUSD } = formData;

	const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};
	const onSubmit = e => {
		e.preventDefault();
		createProduct(formData, history, true);
	};
	const filterErrors = {};
	!isEmpty(errors.list) &&
		errors.list.map(item => {
			if (item.param === 'name') {
				return (filterErrors.name = !isEmpty(name) && item.msg);
			}
			if (item.param === 'priceUSD') {
				if (isEmpty(priceUSD) || isNaN(priceUSD)) {
					filterErrors.priceUSD = item.msg;
				}
			}
			return {};
		});
	return (
		<React.Fragment>
			<form className="wrapper content-area " onSubmit={e => onSubmit(e)}>
				<h1>Edit Product</h1>
				<div className="row errorWrapper">
					<input
						className={`${filterErrors.name && 'error'}`}
						type="text"
						name="name"
						placeholder="Product Name"
						value={name}
						title="productName"
						onChange={e => handleChange(e)}
					/>
					{filterErrors.name && (
						<div className="errorMessage">{filterErrors.name}</div>
					)}
				</div>
				<div className="row errorWrapper">
					<input
						className={`${filterErrors.priceUSD && 'error'}`}
						name="priceUSD"
						placeholder="Price"
						value={priceUSD}
						title="productPrice"
						onChange={e => handleChange(e)}
					/>
					{filterErrors.priceUSD && (
						<div className="errorMessage">{filterErrors.priceUSD}</div>
					)}
				</div>
				<div className="row">
					<Link to="/" className="buttonOne">
						&lt; Back
					</Link>
					<button
						type="button"
						className="buttonOne"
						onClick={() =>
							setFormData({
								name: product.name,
								priceUSD: product.priceUSD
							})
						}>
						Reset
					</button>
					<button type="submit" className="buttonOne">
						Submit
					</button>
				</div>
			</form>
			<form className="wrapper content-area " onSubmit={e => onSubmit(e)}>
				<h1>Edit Product</h1>
				<div className="row errorWrapper">
					<input
						className={`${filterErrors.name && 'error'}`}
						type="text"
						name="name"
						placeholder="Product Name"
						value={name}
						title="productName"
						onChange={e => handleChange(e)}
					/>
					{filterErrors.name && (
						<div className="errorMessage">{filterErrors.name}</div>
					)}
				</div>
				<div className="row errorWrapper">
					<input
						className={`${filterErrors.priceUSD && 'error'}`}
						name="priceUSD"
						placeholder="Price"
						value={priceUSD}
						title="productPrice"
						onChange={e => handleChange(e)}
					/>
					{filterErrors.priceUSD && (
						<div className="errorMessage">{filterErrors.priceUSD}</div>
					)}
				</div>
				<div className="row">
					<Link to="/" className="buttonOne">
						&lt; Back
					</Link>
					<button
						type="button"
						className="buttonOne"
						onClick={() =>
							setFormData({
								name: product.name,
								priceUSD: product.priceUSD
							})
						}>
						Reset
					</button>
					<button type="submit" className="buttonOne">
						Submit
					</button>
				</div>
			</form>
		</React.Fragment>
	);
}

const mapStateToProps = state => ({
	products: state.products,
	loading: state.loading,
	errors: state.errors
});

export default connect(
	mapStateToProps,
	{ createProduct, getProduct }
)(withRouter(Edit));
