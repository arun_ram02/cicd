import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createProduct } from '../../actions';
import { isEmpty } from '../../utls';

function Create({
	createProduct,
	history,

	loading,
	errors
}) {
	const [formData, setFormData] = useState({
		name: '',
		priceUSD: '',
		_id: ''
	});
	const { name, priceUSD } = formData;

	const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};
	const onSubmit = e => {
		console.log('SUBMIT: ', e);
		e.preventDefault();
		e.stopPropagation();
		createProduct(formData, history, false);
	};
	const filterErrors = {};
	!isEmpty(errors.list) &&
		errors.list.map(item => {
			if (item.param === 'name') {
				return (filterErrors.name = name.length === 0 && item.msg);
			}
			if (item.param === 'priceUSD') {
				if (isEmpty(priceUSD) || isNaN(priceUSD)) {
					filterErrors.priceUSD = item.msg;
				}
			}
			return {};
		});

	return (
		<form className="wrapper content-area" onSubmit={e => onSubmit(e)}>
			<h1>Create Product</h1>
			<div className="row errorWrapper">
				<input
					className={`${filterErrors.name && 'error'}`}
					type="text"
					name="name"
					placeholder="Product Name"
					value={name}
					title="productName"
					onChange={e => handleChange(e)}
				/>
				{filterErrors.name && (
					<div className="errorMessage">{filterErrors.name}</div>
				)}
			</div>
			<div className="row errorWrapper">
				<input
					className={`${filterErrors.priceUSD && 'error'}`}
					name="priceUSD"
					placeholder="Price"
					value={priceUSD}
					title="productPrice"
					onChange={e => handleChange(e)}
				/>
				{filterErrors.priceUSD && (
					<div className="errorMessage">{filterErrors.priceUSD}</div>
				)}
			</div>
			<div className="row">
				<Link to="/" className="buttonOne">
					&lt; Back
				</Link>
				<button
					type="button"
					className="buttonOne"
					onClick={() => setFormData({ name: '', priceUSD: '' })}>
					Reset
				</button>
				<button type="submit" className="buttonOne">
					Submit
				</button>
			</div>
		</form>
	);
}

const mapStateToProps = state => ({
	loading: state.loading,
	errors: state.errors
});

export default connect(
	mapStateToProps,
	{ createProduct }
)(withRouter(Create));
