import React from "react";

const NotFound = () => {
	return (
		<div className="wrapper content-area white">
			<h1>
				<i> Page Not Found</i>
			</h1>
			<p>Sorry, this page does not exist</p>
		</div>
	);
};

export default NotFound;
