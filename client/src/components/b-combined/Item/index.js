import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./index.scss";
import { deleteProduct } from "../../../actions";

function Item({ item, deleteProduct, products: { currency, currencyRates } }) {
	let price = item.priceUSD;
	if (currency === "AUD") {
		price = price * currencyRates.USD;
	}
	return (
		<div className="product-item">
			<div className="item-text">
				<div className="row item-name center">
					<h2>{item.name}</h2>
					<div className="item-price">${price.toFixed(2)}</div>
					<Link to={`view/${item._id}`} className="buttonTwo">
						View
					</Link>
					<button
						className="buttonTwo danger"
						onClick={() => deleteProduct(item._id)}>
						Delete
					</button>
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = state => ({
	products: state.products
});

export default connect(
	mapStateToProps,
	{ deleteProduct }
)(Item);

Item.propTypes = {
	item: PropTypes.object.isRequired
};
