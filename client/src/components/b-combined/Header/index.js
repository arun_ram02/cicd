import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';

function Header() {
	return (
		<header>
			<div className="container">
				<div className="wrapper">
					<div className="row">
						<Link to="/" className="logo">
							<h1>A-ONE fashion</h1>
						</Link>
						<Link to="/create" className="buttonOne">
							Create New Product
						</Link>
					</div>
				</div>
			</div>
		</header>
	);
}

export default Header;
