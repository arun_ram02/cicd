// Individual
import Loader from "./a-individual/Loader";

// Combined
import Item from "./b-combined/Item";
import Header from "./b-combined/Header";

// Pages
import Dashboard from "./c-pages/Dashboard";
import Create from "./c-pages/Create";
import View from "./c-pages/View";
import Edit from "./c-pages/Edit";
import NotFound from "./c-pages/NotFound";

export { Dashboard, Item, Loader, Create, View, Edit, NotFound, Header };
