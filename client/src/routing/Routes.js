import React from "react";
import { Route, Switch } from "react-router-dom";
import { Create, Edit, View, NotFound } from "../components";

function Routes() {
	return (
		<section className="container">
			<Switch>
				<Route exact path="/view/:id" component={View} />
				<Route exact path="/create" component={Create} />
				<Route exact path="/edit/:id" component={Edit} />
				<Route component={NotFound} />
			</Switch>
		</section>
	);
}

export default Routes;
